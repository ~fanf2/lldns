-- lldns/generate.lua - generate lldns source files
--
-- Written by Tony Finch <dot@dotat.at> <fanf2@cam.ac.uk>
-- at the University of Cambridge Computing Service
-- http://creativecommons.org/publicdomain/zero/1.0/

local require = require

local io   = require "io"
local lpeg = require "lpeg"
local re   = require "re"

function io.xopen(...)
	local f,e = io.open(...)
	if not f then error(e) end
	return f
end

function io.readfile(name)
	return io.xopen(name):read("*a")
end

-- find next
function lpeg.F(p)
	return lpeg.P{ lpeg.P(p) + 1 * lpeg.V(1) }
end

local function make_parser()
	local env = {}
	local function p(pat)
		env[1] = re.compile(pat,env)
		return lpeg.P(env)
	end
        setfenv(1,env)

	HSpace = (lpeg.S " \t")^1
	VSpace = (lpeg.S"\r\n")^1

	NextLine = lpeg.F(VSpace)

	CComment   = lpeg.P "/*" * lpeg.F "*/"
	CppComment = lpeg.P "//" * NextLine
	HashLine   = lpeg.P "#" * NextLine

	BlankLine = (CComment + HSpace)^0 *
	            (CppComment + HashLine + VSpace)

	Name = p[[ [0-9A-Za-z_]+ ]]

	str2rdf_decl = p[[ <BlankLine>*
	  "ldns_status ldns_str2rdf_" {<Name>}
	    "(ldns_rdf **rd, const char *" <Name> ");" %VSpace
	]]

	str2rdf = p[[ %str2rdf_decl* -> {}
                      <BlankLine>* !. ]]

	return env
end

local parser = make_parser()

local str2rdf = parser.str2rdf:match(io.readfile("ldns/str2host.h"))

local defs = io.xopen("lldns_func_defs.c", "w")
local regs = io.xopen("lldns_func_regs.c", "w")

for i = 1,#str2rdf do
	local n = str2rdf[i]
	local sn = 'str2rdf_'..n
	local ln = 'ldns_'..sn
	local lln = 'l'..ln
	local type = 'LDNS_RDF_TYPE_' .. n:upper()
	defs:write(
	  'static int ',lln,'(lua_State *L) {\n',
	  '	return(lldns_str2rdf(L,',type,',',ln,'));\n',
	  '}\n\n'
	)
	regs:write('{"',sn,'",',lln,'},\n')
end

-- eof
