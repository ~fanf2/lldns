/*
 * lldns - Lua interface to NLnet Labs ldns
 *
 * Written by Tony Finch <dot@dotat.at> <fanf2@cam.ac.uk>
 * at the University of Cambridge Computing Service
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

#include <ldns/ldns.h>

#include "lua.h"
#include "lauxlib.h"

typedef ldns_status lldns_str2rdf_func(ldns_rdf **rd, const char *str);

static int
lldns_str2rdf(lua_State *L,
    ldns_rdf_type type, lldns_str2rdf_func *func) {
	const char *str = lua_tostring(L, 1);
	ldns_rdf *rdf = NULL;
	ldns_status status = func(&rdf, str);
	if (LDNS_STATUS_OK != status) {
		LDNS_FREE(rdf);
		lua_pushstring(L, ldns_get_errorstr_by_id(status));
		return(lua_error(L));
	}
	size_t size = ldns_rdf_size(rdf);
	ldns_rdf *lrdf = lua_newuserdata(L, sizeof(*rdf) + size);
	void *data = lrdf + 1;
	ldns_rdf_set_size(lrdf, size);
	ldns_rdf_set_type(lrdf, type);
	ldns_rdf_set_data(lrdf, data);
	memcpy(data, ldns_rdf_data(rdf), size);
	ldns_rdf_deep_free(rdf);
	return(1);
}

#include "lldns_func_defs.c"

static const luaL_Reg functions[] = {
	#include "lldns_func_regs.c"
	{ NULL, NULL }
};

LUALIB_API int
luaopen_lldns(lua_State *L) {
	luaL_register(L, "ldns", functions);
	return(1);
}

/* eof */
