# lldns - Lua interface to NLnet Labs ldns
#
# Written by Tony Finch <dot@dotat.at> <fanf2@cam.ac.uk>
# at the University of Cambridge Computing Service
# http://creativecommons.org/publicdomain/zero/1.0/

CFLAGS = -pipe -O -g -std=c99 -pedantic \
	-Wall -Wextra -Wbad-function-cast -Wcast-align \
	-Wformat=2 -Winit-self -Winline -Wnested-externs \
	-Wold-style-definition -Wpointer-arith -Wredundant-decls \
	-Wshadow -Wswitch-default -Wswitch-enum \
	-Wunreachable-code -Wwrite-strings \
	-I/opt/lua/include -I/opt/ldns/include

LDFLAGS=-shared -fpic -L/opt/ldns/lib -Wl,-R,/opt/ldns/lib

all : lldns.so

lldns.so : lldns.o
	${CC} ${LDFLAGS} -o $@ lldns.o -lldns

lldns.o : lldns.c lldns_func_defs.c lldns_func_regs.c

lldns_func_defs.c lldns_func_regs.c : ldns generate.lua
	lua generate.lua

clean:
	rm -f lldns.so lldns.o lldns_func_defs.c lldns_func_regs.c

up:
	git gc
	git update-server-info
	touch .git/git-daemon-export-ok
	echo "Lua interface to NLnet Labs ldns" >.git/description
	rsync --delete --recursive --links .git/ chiark:public-git/lldns.git/

# eof
